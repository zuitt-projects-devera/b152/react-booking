import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css'

/*
  ReactJS does not like rendering two adjacent elements. Instead the adhacent elements must he wrapped by a
  parent element or React Fragments
  (<>...</>

  If the parent of the elements is an actual HTML element, it will be added in the HTML/DOM

  If the parent of the elements is a React Fragment (<></>), React will not be added in the HTML/DOM.*/



/*Embedding JS expressions in react elements with JSX

  */


// let myName = (
//   <>
//     <h1>Hello World!</h1>
//     <h2>Andrae</h2>
//   </>
//   )


// let name = "Jose"
// let job = "Superhero"


// let num1 = 100;
// let num2 = 250;

// let numDisplay = (
//   <>
//   <h1>The sum of {num1} and {num2} is: </h1> 
//   <h2>{num1 + num2}</h2>
//   </>
//   )

// let arrayOfStrings = ["blue", "andrae", "de vera"];

// let arrayOfObjs = [
//   {
//   name: "Saitama",
//   job: "Hero",
//   age: 24,
//   address: "Z-City",
//   income: 100000,
//   expense: 50000,
//   currentbalance: 760000
//   },
//   {
//   name: "Boros",
//   job: "Villain",
//   age: 1907,
//   address: "Nebula Galaxy",
//   income: 241221,
//   expense: 114242,
//   currentbalance: 75421
//   }


// ]

/*
let arrayOfObsDisplay = (
  <>
  <table>
  {arrayOfObjs.map(names => (
   <tr>{names.names}</tr>
   <tr>{names.job}</tr>
 
  ))}
  </table>
  </>
)*/

// let arrayDisplay = (
//   <>
//   <ul>
//   {arrayOfStrings.map(names => (
//    <li>{names}</li>
 
//   ))}
//   </ul>
//   </>
// )

// let personDisplay1 = (
//   <React.Fragment>
//   <h1>{myName}</h1>

//   </React.Fragment>

//   )

// let hero1 = {
//   name: "Saitama",
//   job: "Hero",
//   age: 24,
//   address: "Z-City",
//   income: 100000,
//   expense: 50000,
//   currentbalance: 760000
// }

// let objDisplay = <p>Hi! My name is {hero1.name}. I am a {hero1.age} years old. I work as a {hero1.job} at Superhero agency.</p>

// let aheroforfun = (
//   <>
//   <h1>Name: {hero1.name}</h1>
//   <h2>Job: {hero1.job}</h2>
//   <h3>Age: {hero1.age}</h3>
//   <h3>Address: {hero1.address}</h3>
//   <h4>Income: ¥{hero1.income}</h4>
//   <h4>Expenses: ¥{hero1.expense}</h4>
//   <h4>Current Balance: ¥{hero1.income - hero1.expense}</h4>
//   </>
//   )

 // const SampleComp = () => {

   // return (
  
      //  <h1>I am a react element returned by a function.</h1>
  
     // )
  
 // }
  

/*  The syntax used in ReactJS in JSX.

JSX = Javascript + XML, It is an extension of Javascript that let's us create object which wil be compiled
as HTML elementts.

With JSX, we are able to create HTML elements using JS. */

//console.log(element);

/*ReactDOM.render() - allows us to render/display our reactjs elements and display in our html.

ReactDOM.reander(<reactElement>,<htmlElementSelectedById>)*/

/*How are components called? 
Create a self-closing tag </SampleComp> to call your component.

In ReactJS, we normally render our components in an entry point or ina mother component called App. This is so
we can group our components under a single entry point/main component

All other components/pages will be contained in our main component: <App/>

react.StrictMode is a built-in react component which is used to highlight potential problems in our code and
in fact allows for more information about errors in our code.*/

ReactDOM.render(
  <React.StrictMode>
    <App />  
  </React.StrictMode>,
  document.getElementById('root')
);
