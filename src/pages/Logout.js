import {useContext, useEffect} from 'react';
import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';

export default function LogOut() {
	//destructure our UserContext and get our setUser and unsetUser function from our context.
	const {setUser, unsetUser} = useContext(UserContext);

	//clear the localStorage
	unsetUser();

	//add the useEffect to run our setUser. This useEffect will have an empty dependency array .
	useEffect(() => {
		
		//update our global use state to its initial values. 
		setUser({
			id:null,
			isAdmin: null
		})

	}, [])
	return (
		<Navigate to='/login' replace ={true} />
		)


}