import {useState, useEffect, useContext} from 'react';
import {Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';


export default function ViewCourse({CourseProp}) {
	/*fetch the details of our course by its id. 
	
	useParams from react-router-dom will allow us to retrieve the data from our Browser URL.
	useParams() will return an object containing URL params.
	*/
	//console.log(useParams());
	//destructure the object from useParams() and save it in a variable.
	const {courseId} = useParams();

	let token = localStorage.getItem('token');
	//get the global user state from our context:
	const {user} = useContext(UserContext);


	const [courseDetails, setCourseDetails] = useState({
		name: null,
		description: null,
		price: null
	})

	useEffect(() => {
		fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			setCourseDetails({
				name: data.name,
				description: data.description,
				price: data.price
			})
		})
	}, [courseId])
	
	function enroll() {
		/*console.log("enroll");*/
		//console.log(courseId);
		Swal.fire({
				title: 'You sure you want to enroll?',
				text: "Yes, I'm sure.   ",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then((result) => {
				if (result.isConfirmed) {
					fetch('http://localhost:4000/users/enrollUser',
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`            
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message === "Enrolled Successfully.") {
						Swal.fire({
							icon: "success",
							title: "Enrollment Successful",
							text: `You've successfully added ${courseDetails.name} to your courses.`
						})
						}
						else {
							Swal.fire({
							icon: "error",
							title: "Something went wrong.",
							text: data.message
						})
						}
		})     
				}
				})
		
	}
	return (
		<Row className = "mt-5">
		<Col>
		<Card>
		<Card.Body className="text-center">
		<Card.Title>{courseDetails.name}</Card.Title>
		<Card.Subtitle>Description:</Card.Subtitle>
		<Card.Text>{courseDetails.description}:</Card.Text>
		<Card.Subtitle>Price:</Card.Subtitle>
		<Card.Text>{courseDetails.price}:</Card.Text>
		</Card.Body>
		{
			user.id
			? <Button variant="primary" className="btn-block" onClick={()=>{enroll()}}> Enroll </Button>
			: <Link className="btn btn-danger btn-block" to="/login" >Login to Enroll</Link>
		}
		</Card>
		</Col>
		</Row>

		)
}