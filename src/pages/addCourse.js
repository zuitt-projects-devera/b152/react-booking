import {useState} from "react";
import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';
export default function AddCourse() {

   const [name, setName] = useState("");
   const [description, setDescription] = useState("");
   const [price, setPrice] = useState("");

   function addCourse(e) {
    let token = localStorage.getItem('token');
       //let role = localStorage.getItem('isAdmin');
       e.preventDefault();
        //console.log(role);
        Swal.fire({
            title: 'You sure you want to add this course?',
            text: "Yes, I'm sure.   ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch('http://localhost:4000/courses/',
                {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`          
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if(data._id) {
                        Swal.fire({
                            icon: "success",
                            title: "Course Added Successfully",
                            text: `Successfully added ${name} to courses.`
                        })          
                    }
                    else {
                        Swal.fire({

                            icon: "error",
                            title: "Couldn't add this course.",
                            text: "Something went wrong."

                        })
                    }
                })

            }
            })
        }

        return (
            <>
            <h1>Add New Course</h1>
            <Form onSubmit = {e => addCourse(e)}>
            <Form.Group>
            <Form.Label>Course Name: </Form.Label>             
            <Form.Control type="text" placeholder="Enter First Name" value={name} 
            onChange={e => {setName(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
            <Form.Label>Course Description: </Form.Label>
            <Form.Control type="text" placeholder="Enter Last Name" value={description} 
            onChange={e => {setDescription(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
            <Form.Label>Course Price: </Form.Label>
            <Form.Control type="text" placeholder="Enter Email" value={price} 
            onChange={e => {setPrice(e.target.value)}} required></Form.Control>
            </Form.Group>
            <Button variant="primary" type="submit" className="my-5">Add Course</Button>
            </Form>
            </>
            )
    }