import Banner from '../components/Banner';
import CourseCard from '../components/Course';
import {useState, useContext, useEffect} from 'react';
import UserContext from '../userContext';

import AdminDashboard from '../components/AdminDashboard'

export default function Courses() {


const {user} = useContext(UserContext);

   let coursesBanner = {
    title: "Welcome to the Courses Page",
    description: "View one of our courses below",
    buttonText: "Register/Login to Enroll",
    destination: "/register"
}

const [coursesArray, setCoursesArray] = useState([]);

//create useEffect which will retrieve our courses on initial render.
useEffect(() => {
    /*fetch all ACTIVE courses
      For requests that are GET method AND does not need to pass token,
      we don't need to add {options}*/
    fetch("http://localhost:4000/courses/getActiveCourses")
    .then(res => res.json())
    .then(data => {

        setCoursesArray(data.map(courses => {
            return (

                <CourseCard key={courses._id} CourseProp = {courses} />
                )
        }));
            
         })
}, [])

	/*
		Components are independent from one another. It may come from the same component file but when returned each component is independent from one another. 

		Remember, components are functions. We run the same function multiple times.

       */


       /*Create an array of CourseCard components by mapping out details from our coursesData array*/
   /* let coursesComponents = coursesData.map(courses => {
        return <CourseCard CourseProp = {courses} key={courses.id}/>
    })*/
    //we essentially returned an array of CourseCard components into our new courseComponents.

	//We are also able to pass the data of the current item being looped by map(), by passing the course parameter in our anonymous function

	//When we create an array of components, we have to add a unique identifier for each component. Pass a key prop with a unique identifier for each item in the array

    return (
        user.isAdmin 
        ? <AdminDashboard />
        :
        <>

        <Banner bannerProp={coursesBanner}/>
        {coursesArray}
        </>
        )
    }
    