/* Our home page component will be the parent component of our banner and highlights components. */
import Banner from "../components/Banner";
import Highlights from '../components/Highlights';
import About from '../components/About';

export default function Home() {

        let bannerData = {
            title: "Zuitt Booking System B152",
            description: "View and book a course from our catalog.",
            buttonText: "View Our Courses",
            destination: "/courses"
        }
        
        /* ReactHS adheres to the comcept of D.R.Y. - Dont Repeat Yourself
        
        Components in ReactJS are independent and reusable.

        What makes a ReactJS component reusable is with the use of props.

        Props are data we can pass from a parent component to a child component.
        
        Parent components are components which return other components.    

        Child components are components returned by a parent component.
        */
       /* let sampleProp = "This is a string" */
        let sampleProp2 = "This sample data is passed form Home to Highlights component."
        /* To pass prop from a parent component to a child component, we add HTML-like attribute
         to the child component which we can name ourselves. Props are html-like attributes we can name 
         ourselves.
         
            The name of the attribute will become the name of a property of the object that the 
            component receives. That is why this is called Props. Props stands for properties.*/

 return (

            
                <>
                <Banner bannerProp={bannerData}/>
                <Highlights highLightsProp={sampleProp2}/>
    
            </>
        
            

    )                              
}