import {useState, useContext, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

/*Import Navigate component from react-router-dom, this will allow us to Redirect our users after logging in 
and updating the global user state. If a logged in user tries to go to our logijn page via browser, they
will be redirected to our home page. */
import {Navigate} from 'react-router-dom';

export default function Login() {
   
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    //state for conditional rendering our button
    const [isActive, setIsActive] = useState(false);
    //false is the initial value because initially, the form is empty and the button must be disabled.

    /* add useEffect to check if the user is able to fill up our form. As long as both inputs are not filled, 
    the submit button is disabled, else, it will be enables.
    
    This useEffect will run the states in the dependecy array are updated.

    */

    useEffect(()=> {
        if(email !== "" && password !== "") {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }


    }, [email, password])



    function loginUser(e) {

        e.preventDefault();


        fetch('http://localhost:4000/users/login',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"            
            }, 
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
          // console.log(data);
            if(data.accessToken) { 
                Swal.fire({

                icon: "success",
                title: "Login Successful",
                text: `Welcome, thank you for logging in.`
            })
            /* save our accessToken when log in successfully. We will save it in our localStorage.
            localStorage is an object in JS which allows us to save smaill amounts of data within
            our browser. We can use this to save our token.
            localStorage.exists in most browsers.
            localStorage.setItem() will allow us to save data in our browsers.
            However, any data we pass to localStorage will become as tring.
            //syntax: localStorage.setItem(<key>, <value>) */
            localStorage.setItem('token', data.accessToken)
            let token = localStorage.getItem('token');
           // console.log(token);
           // localStorage.setItem('token', "sample message");
            fetch('http://localhost:4000/users/getUserDetails', {
                method: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`             
                }
            })
            .then(res => res.json())
            .then(data => {

                /*localStorage.setItem('id', data._id);
                localStorage.setItem('isAdmin', data.isAdmin);*/
                //update the global user state with the id and isAdmin details of our user
                setUser({
                        id: data._id,
                        isAdmin: data.isAdmin
                    })
            })
        }
        else {
                Swal.fire({
                icon: "error",
                title: "Login Failed",
                text: data.message
    
            })
        }       
        })
    }

//create a ternary to redirect our user if they are logged in. If not, we will show our form
    return (
        user.id
        ?
        <Navigate to="/courses" replace={true} />
        :
        <>
        <h1>Login</h1>   
        <Form onSubmit = {e => loginUser(e)}>
            <Form.Group>
                <Form.Label>Email: </Form.Label>             
                <Form.Control type="text" placeholder="Enter Email" value={email} 
                onChange={e => {setEmail(e.target.value)}}required></Form.Control>
            </Form.Group>
           
            <Form.Group>
                <Form.Label>Password: </Form.Label>
                <Form.Control type="password" placeholder="Enter Password" value={password} 
                onChange={e => {setPassword(e.target.value)}}required></Form.Control>
            </Form.Group>
            {
                isActive
                ?  <Button variant="primary" type="submit" className="my-5">Login</Button>
                :  <Button variant="primary" disabled className="my-5">Login</Button>
            }
           
        </Form>
        </>
    )
}