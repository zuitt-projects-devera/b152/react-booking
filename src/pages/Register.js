import {useState, useContext, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../userContext';

import Swal from 'sweetalert2';

import {Navigate} from 'react-router-dom';

export default function Register() {

   const [firstName, setFirstName] = useState("");
   const [lastName, setLastName] = useState("");
   const [email, setEmail] = useState("");
   const [mobileNo, setMobileNo] = useState("");
   const [password, setPassword] = useState("");
   const [confirmPassword, setConfirmPassword] = useState("");
   const [isActive, setIsActive] = useState(false);

   const {user, setUser} = useContext(UserContext);
   /* Two Way Binding
   
    To be able to capture/save the input value from our input elements, we can bind the
    value of our element with our states. We cannot type into our inputs anymore
    because there is now value that is bound to it. We will add an onChange event
    to be able to update the state that was bound  to our input.
    
    Two Way Binding is done so that we can assure that we can save the input into our states
    as we type into the input element. This is so we don't have to savie it just before submit.
    
    
    <Form.Control type="inputType" value={inputState} onChange = {e => {setInputState(e.target.value)}}
    
    e = event, all event listeners can pass the event object. The event object contains all the 
    details about the event,
    
    
    */
     useEffect(()=> {
        if(firstName !== "" 
            && lastName !== "" 
            && email !== "" 
            && mobileNo.length === 11
            && password === confirmPassword
            ) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }


    }, [firstName, lastName, email, mobileNo, password, confirmPassword])


    function registerUser(e) {
        //prevent submit event's default behavior
        e.preventDefault();


        /* //fetch() is a JS method which allow us to pass/create a request to an api.
		//syntax: fetch("<requestURL>",{options})
		//method: the request's HTTP method.
		//headers: additional information about our request.
		//"Content-Type": "application/json" denotes that the request body contains JSON.
		//we JSON.stringify the body of our request to be able to send a JSON format string to our API.
         */
        fetch('http://localhost:4000/users/',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"            
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {/* data is the response of the api/server after it's been process
        as JS object through our res.json() method. */

            if(data.email) { //data will only contain an email property if we can properly save our user.
                Swal.fire({

                    icon: "success",
                    title: "Registration Successful",
                    text: "Thank you for registering."

                })
            }
            else {
                Swal.fire({

                    icon: "error",
                    title: "Registration Failed",
                    text: data.message 

                })
            }
        })
 
    }

    return (
         user.id
        ?
        <Navigate to ="/courses" replace={true} />
        :
        <>
        <h1>Register</h1>
        <Form onSubmit = {e => registerUser(e)}>
            <Form.Group>
                <Form.Label>First Name: </Form.Label>             
                <Form.Control type="text" placeholder="Enter First Name" value={firstName} 
                onChange={e => {setFirstName(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Last Name: </Form.Label>
                <Form.Control type="text" placeholder="Enter Last Name" value={lastName} 
                onChange={e => {setLastName(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Email Name: </Form.Label>
                <Form.Control type="text" placeholder="Enter Email" value={email} 
               onChange={e => {setEmail(e.target.value)}} required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Mobile Number: </Form.Label>
                <Form.Control type="text" placeholder="Enter Mobile Number" value={mobileNo} 
                onChange={e => {setMobileNo(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Password: </Form.Label>
                <Form.Control type="password" placeholder="Enter Password" value={password} 
                onChange={e => {setPassword(e.target.value)}}required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Confirm Password: </Form.Label>
                <Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} 
                onChange={e => {setConfirmPassword(e.target.value)}}required></Form.Control>
            </Form.Group>
            {isActive 
            ? <Button variant="primary" type="submit" className="my-5">Register</Button>
            : <Button variant="primary" disabled className="my-5">Register</Button>}
            
        </Form>
        </>


    )
}