//import react bootstrap components
//react-bootstrap components are react components that create/return react elements with the appropriate bootstrap classes.

import {useState} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap'

/*import Link component - Link component is a component from react-router-dom so that
we will simply switch the pages and not refresh the page when link to a different a different page.
*/
import {Link} from 'react-router-dom';

export default function CourseCard({CourseProp}){
   /* console.log(CourseProp); */
	/*To be able to add a class in your react elements and react bootstrap components, we add className, instead of class*/



 /*By convention, create your states using useState and keep in mind to add them INSIDE the component
      and at the top of the component itself. */


      const [count, setCount] = useState(0);
      const [seats,setSeats] = useState(30);
      /*const [btndisable, btnsetDisable] = useState(false);// button state */



      //useState react hook returns an array which contains the state and the setter function.
      //count was destructured from our useState() and its initial value is the argument added to useState() 


      //console.log(count);
    /* States in Reactjs are ways to store information within a component. The advantage of a state from a 
    variable is that especially within a component, variables do not retain updated information when the 
    component is updated. 

    Creating a state: 

    useState() hook from react will allow us to create a state and its setter function.   */


    /* We will attach the enroll function on a click event we will add on our button
    whenever a button is clicked the enroll function will run and the count state
    will be updated by the setter function */
 
    function enroll() {
  
            setCount(count + 1);
            setSeats(seats - 1);

         /*    if(seats === 1) { //btnsetstate
                btnsetDisable(true);
       
            } */


    }

 /* Conditional rendering is the ability to display or hide elements  */

 /*We will pass the id of the course in the Link component, we will pass the id of the course in
 the browser URL as params. */

 //console.log(CourseProp);
	return (

		<Row>
			<Col xs={12} >
                <Card className = "p-3 mt-3">
                    <Card.Body>
                        <Card.Title>
                        {CourseProp.name}
                        </Card.Title>
                        <Card.Text>
                        {CourseProp.description}
                              
                        </Card.Text>
                        <Card.Text>
                        Price: {CourseProp.price}
                        </Card.Text>
                     
                        <Link to={`/courses/ViewCourse/${CourseProp._id}`} className="btn btn-primary">View Course</Link>
                     
                        
                        {/*<Card.Text>
                        Enrollees: {count === 0 
                        ? <span className= "text-danger">No enrollees yet.</span>
                        : <span className= "text-Secondary">{count}</span>
                        }
                        </Card.Text>

                        <Card.Text>
                        Seats: {seats === 0 
                        ?<span className= "text-danger">No more seats available.</span>
                        : <span className= "text-success">{seats}</span>}
                        </Card.Text>
                        {seats === 0
                        ?  <Button variant="primary"disabled>Enroll Now</Button>
                        : <Button variant="primary" onClick={enroll}>Enroll Now</Button>}*/}

                       {/*  <Button variant="primary" onClick={enroll} disabled={btndisable}>Enroll Now</Button> not conditional rendering*/}
                       
                    </Card.Body>                   
                </Card>                
			</Col>
            
		</Row>

	)

}
