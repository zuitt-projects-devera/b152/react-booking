
import {Button, Row, Col} from 'react-bootstrap'
export default function Banner(){

	/*To be able to add a class in your react elements and react bootstrap components, we add className, instead of class*/

	return (

		<Row>
			<Col className="p-5">
                <div className="p-5 bg-secondary">
				<h1 className="my-5">About Me</h1>
				<h2 className="mt-3">Blue Andrae De Vera</h2>
				<h3 className="mt-3">Full Stack Web Developer</h3>
                <p className="mb-4">I'm a Full Stack Web Developer with the following
                Tech Stack:</p>
                <ul>
                    <li>MongoDB</li>
                    <li>ExpressJS</li>
                    <li>ReactJS</li>
                    <li>NodeJS</li>
                </ul>
                <h2 className="mt-3">Should you wish to have collaboration, click the button below.</h2>
                <Button variant="primary mt-5">Contact Me</Button>
                </div>
			</Col>
		</Row>

	)

}
