//import react bootstrap components
//react-bootstrap components are react components that create/return react elements with the appropriate bootstrap classes.
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights({highLightsProp}){
    /* console.log(highLightsProp); */
	/*To be able to add a class in your react elements and react bootstrap components, 
    we add className, instead of class*/

 

	return (

		<Row>
			<Col xs={12} md={4}>
                <Card className = "cardHighlight p-3 mt-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn From Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur dolore qui anim officia sit excepteur 
                            minim pariatur dolore ea sint irure do. Minim non qui
                             reprehenderit ipsum. Eu eu quis anim laborum aute esse
                              proideSunt id magna consectetur mollit mollit aliquip 
                              
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3  mt-3">
                    <Card.Body>
                        <Card.Title>
                            <h2> Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur dolore qui anim officia sit excepteur 
                            minim pariatur dolore ea sint irure do. Minim non qui
                             reprehenderit ipsum. Eu eu quis anim laborum aute esse
                              proident quis laborum sunt pariatur dolore. Dolore excepteur 
                              officia laboris adipisicing.
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
            <Col xs={12} md={4}>
                <Card className = " cardHighlight p-3  mt-3">
                    <Card.Body>
                        <Card.Title>
                            <h2> Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur dolore qui anim officia sit excepteur 
                            minim pariatur dolore ea sint irure do. Minim non qui
                             reprehenderit ipsum. Eu eu quis anim laborum aute esse
                              proident quis laborum sunt pariatur dolore. Dolore excepteur 
                              officia laboris adipisicing enim ex excepteur irure ex occaecat 
                              ullamco. Enim aliquip culpa est adipisicing ex consectetur ullamco 
                              qui elit. Deserunt.
                        </Card.Text>
                    </Card.Body>                   
                </Card>                
			</Col>
		</Row>

	)

}
