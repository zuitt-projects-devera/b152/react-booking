//import react bootstrap components
//react-bootstrap components are react components that create/return react elements with the appropriate bootstrap classes.
import {Button, Row, Col} from 'react-bootstrap'


//destructure the special object receive and get only the props we need.
export default function Banner({bannerProp}){
	/* 	console.log(bannerProp) */

		/* All components are children to other components can receive a special object. We can 
	receive it here as a parameter.
	
	*/

	/*To be able to add a class in your react elements and react bootstrap components, 
	we add className, instead of class*/

	
	return (

		<Row>
			<Col className="p-5">
				<h1 className="mb-3">{bannerProp.title}</h1>
				<p className="my-3">{bannerProp.description}</p>
				<a href = {bannerProp.destination} className = "btn btn-primary">{bannerProp.buttonText}</a>
			</Col>
		</Row>

	)

}
