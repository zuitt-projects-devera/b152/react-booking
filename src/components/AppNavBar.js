import {useContext} from 'react';

//import react-bootstrap components:
import {Nav,Navbar,Container} from 'react-bootstrap'

import UserContext from '../userContext';


export default function AppNavBar(){


	/* 
	
	useContext() hook will allow us to unwrap the values provided by our UserProvider 
	from our UserContext. userContext() will return an object after unwrapping our context.
	
	*/
	//console.log(useContext(UserContext));

	//descturcture our UserContext to get the global user state.
	const {user} = useContext(UserContext);

	return (



			<Navbar bg="secondary" expand="lg" >
				<Container fluid>
					<Navbar.Brand href="/">BA</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="ml-auto">
							<Nav.Link href="/">Home</Nav.Link>
							<Nav.Link href="/courses">Courses</Nav.Link>
							{
								user.id
								?
									user.isAdmin
									?
									<>
										<Nav.Link href="/addCourse">Add Course</Nav.Link>
										<Nav.Link href="/logout">Logout</Nav.Link>
									</>
									:
									<Nav.Link href="/logout">Logout</Nav.Link>
								:
								<>
									<Nav.Link href="/register">Register</Nav.Link>
									<Nav.Link href="/login">Login</Nav.Link>
								</>	
							}
                         
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>


		)

}
